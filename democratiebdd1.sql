-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mar. 31 août 2021 à 07:29
-- Version du serveur :  5.7.24
-- Version de PHP : 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `democratiebdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `Id_comments` int(11) NOT NULL,
  `text` varchar(100) DEFAULT NULL,
  `horaire` datetime DEFAULT NULL,
  `Id_users` int(11) NOT NULL,
  `Id_propositions` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `propositions`
--

CREATE TABLE `propositions` (
  `Id_propositions` int(11) NOT NULL,
  `titre` varchar(50) DEFAULT NULL,
  `deja_vote` tinyint(1) DEFAULT NULL,
  `pros` varchar(255) NOT NULL,
  `soumission` tinyint(1) DEFAULT NULL,
  `cons` smallint(6) DEFAULT NULL,
  `Id_users` int(11) NOT NULL,
  `pour` smallint(5) UNSIGNED NOT NULL DEFAULT '1',
  `contre` smallint(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `propositions`
--

INSERT INTO `propositions` (`Id_propositions`, `titre`, `deja_vote`, `pros`, `soumission`, `cons`, `Id_users`, `pour`, `contre`) VALUES
(1, 'salut', NULL, '', NULL, NULL, 16, 3, 0),
(2, 'dvss', NULL, '', NULL, NULL, 16, 1, 0),
(3, 'salut', NULL, 'ghehjerj', NULL, NULL, 16, 2, 0),
(4, 'salut cnf', NULL, 'dd', NULL, NULL, 16, 2, 0),
(5, 'salutneryal', NULL, 'est ce que Ã§a marche ', NULL, NULL, 17, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `Id_users` int(11) NOT NULL,
  `pseudo` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `actif` tinyint(1) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`Id_users`, `pseudo`, `password`, `actif`, `email`) VALUES
(13, 'toto', '$2y$10$CekPddTp0COjhW6blqT.B.cP6dN/KutK2vIyoxoAXmzSvd19DC6Pa', 0, 'tototo@yopmail.com'),
(14, 'ff', '$2y$10$R2q0lGk59p970rRtjAXFxenNBUVnCUC.7QrcNvcscNdr1z/K0m0VG', 0, 'tototo@yopmail.com'),
(15, 'tototo', '$2y$10$BdZWtrpvP.KylC100AEr1O9oqSbO59uqDbdXpLQUVHu1wPGYiZ7yS', 0, 'tototo@yopmail.com'),
(16, 'neryal', '$2y$10$zBpNx7sHZdegtWWI8rE.0.Yye1d5HoW7oHj8FCzs/jQWQvHORgYe2', 0, 'tototo@yopmail.com'),
(17, 'toto1', '$2y$10$gCzHyLPD3bPRAatYAgjF2.92s9OVYciIROxrL8xZuJP22U3RrfMLS', 0, 'tototo@yopmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `vote`
--

CREATE TABLE `vote` (
  `Id_users` int(11) NOT NULL,
  `Id_propositions` int(11) NOT NULL,
  `a_vote` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `vote`
--

INSERT INTO `vote` (`Id_users`, `Id_propositions`, `a_vote`) VALUES
(16, 1, NULL),
(16, 4, NULL),
(17, 1, NULL),
(17, 3, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`Id_comments`),
  ADD KEY `Id_users` (`Id_users`),
  ADD KEY `Id_propositions` (`Id_propositions`);

--
-- Index pour la table `propositions`
--
ALTER TABLE `propositions`
  ADD PRIMARY KEY (`Id_propositions`),
  ADD KEY `Id_users` (`Id_users`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id_users`);

--
-- Index pour la table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`Id_users`,`Id_propositions`),
  ADD KEY `Id_propositions` (`Id_propositions`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `Id_comments` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `propositions`
--
ALTER TABLE `propositions`
  MODIFY `Id_propositions` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `Id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`Id_users`) REFERENCES `users` (`Id_users`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`Id_propositions`) REFERENCES `propositions` (`Id_propositions`);

--
-- Contraintes pour la table `propositions`
--
ALTER TABLE `propositions`
  ADD CONSTRAINT `propositions_ibfk_1` FOREIGN KEY (`Id_users`) REFERENCES `users` (`Id_users`);

--
-- Contraintes pour la table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `vote_ibfk_1` FOREIGN KEY (`Id_users`) REFERENCES `users` (`Id_users`),
  ADD CONSTRAINT `vote_ibfk_2` FOREIGN KEY (`Id_propositions`) REFERENCES `propositions` (`Id_propositions`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
