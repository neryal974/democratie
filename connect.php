<?
require_once 'env.php';
require_once 'fctDebug.php';

//Bloc try/catch pour attraper l'erreur s'il y a un problème de connexion avec la DB
try {  
  $db = new PDO("mysql:host=$db_hostname; dbname=$db_database;", $db_username, $db_password); //On se connecte à la DB en utilisant les données présentes dans env.php
  $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); //Je configure le fetch mode en tableaux associatif par défaut
} catch (PDOException $e) { //Si Exception (erreur) attrapée (catch) par PHP)  
  echo "Erreur!: " . $e->getMessage() . "<br/>"; //on affiche le message   
  die();}

  /**
 * Cette fonction permet d'exécuter tout type de requete SQL de manière générique
 * 
 * $sql sera la requete SQL (ex : SELECT * FROM users)
 * $fetchMany nous permet de savoir si on doit renvoyer une seule ligne ou bien plusieurs
 */

function runQuery($sql, $params = [], $fetchMany = true){ // On peut définir une valeur par défaut pour les paramètres en PHP
  global $db; //Attention : on récupère la variable $db initialisé à l'extérieur de cette fonction (problème de scope)
  
  $query = $db->prepare($sql); //preparation de la requête
  $query->execute($params); //Ajout des paramètres + exécution de la requete

  if($fetchMany === false){ //Si on attend qu'un seul résultat
      return $query->fetch(); //On utilise fetch()
  } else { //Si on attends plusieurs résultats
      return $query->fetchAll(); //On utilise fetchAll()
  }
}

//On va chercher les infos sur l'article
//$post = runQuery('SELECT * FROM posts WHERE id = :post_id', [':post_id' => $post_id], false);