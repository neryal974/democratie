-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : lun. 19 juil. 2021 à 03:37
-- Version du serveur : 5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `democratiebdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `Id_comments` int(11) NOT NULL,
  `comment` varchar(50) DEFAULT NULL,
  `dateHeure` datetime DEFAULT NULL,
  `Id_users` int(11) NOT NULL,
  `Id_propositions` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `propositions`
--

CREATE TABLE `propositions` (
  `Id_propositions` int(11) NOT NULL,
  `titre` varchar(50) DEFAULT NULL,
  `text` varchar(100) DEFAULT NULL,
  `soumission` tinyint(1) DEFAULT '0',
  `pour` smallint(6) DEFAULT '1',
  `contre` smallint(6) DEFAULT '0',
  `Id_users` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `propositions`
--

INSERT INTO `propositions` (`Id_propositions`, `titre`, `text`, `soumission`, `pour`, `contre`, `Id_users`) VALUES
(1, 'premier prop', 'truc de ouf sa marche pas ', 0, 1, 1, 7),
(2, 'jufebzouiguognbzoignz', 'ojuzgbouzgbuzoigbogbzolgnklzmg', 0, 2, 0, 7),
(5, 'nrjrj', 'jrjrj', 0, 1, 1, 7),
(6, 'hhhrh', 'rhrhr', 0, 1, 0, 7),
(12, '', '', 0, 1, 0, 7),
(13, 'hhhrh', 'hh', 0, 1, 0, 7),
(14, 'hhhrh', 'ca marche?', 0, 1, 0, 7),
(15, 'hhhrh', 'bbbb', 0, 1, 0, 16),
(16, 'nouvelle proposition ', 'la voici', 0, 1, 0, 7),
(17, 'pour sam ', 'jespÃ¨re que tu le verrra', 0, 1, 0, 7),
(18, 'je les vue !!', 'Ã§a fonctionne ', 0, 3, 0, 16),
(19, 'salut tes du vo-e', 'faffzfzgfzgzgzrgzgzgzgzgzgzgzgz', 0, 2, 0, 16);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `Id_users` int(11) NOT NULL,
  `pseudo` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `actif` tinyint(1) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`Id_users`, `pseudo`, `password`, `actif`, `email`) VALUES
(7, 'antho', '$2y$10$MFBuQpT0KfZom4l06Zi93.zyNZRJD3wA0RoSIMtX747vgl4vgjQSC', 0, 'anthonymaillot97411@gmail.com'),
(16, 'sam', '$2y$10$9c3lf4TkKRsMLef1HNA8xO3Rc78X3Biz3V1Bj6/W2YpwKeuk/gicO', 1, 'sam@yopmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `voter`
--

CREATE TABLE `voter` (
  `Id_users` int(11) NOT NULL,
  `Id_propositions` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `voter`
--

INSERT INTO `voter` (`Id_users`, `Id_propositions`) VALUES
(16, 1),
(16, 2),
(16, 5),
(7, 18),
(7, 19);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`Id_comments`),
  ADD KEY `Id_users` (`Id_users`),
  ADD KEY `Id_propositions` (`Id_propositions`);

--
-- Index pour la table `propositions`
--
ALTER TABLE `propositions`
  ADD PRIMARY KEY (`Id_propositions`),
  ADD KEY `Id_users` (`Id_users`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id_users`);

--
-- Index pour la table `voter`
--
ALTER TABLE `voter`
  ADD PRIMARY KEY (`Id_users`,`Id_propositions`),
  ADD KEY `Id_propositions` (`Id_propositions`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `Id_comments` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `propositions`
--
ALTER TABLE `propositions`
  MODIFY `Id_propositions` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `Id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`Id_users`) REFERENCES `users` (`Id_users`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`Id_propositions`) REFERENCES `propositions` (`Id_propositions`);

--
-- Contraintes pour la table `propositions`
--
ALTER TABLE `propositions`
  ADD CONSTRAINT `propositions_ibfk_1` FOREIGN KEY (`Id_users`) REFERENCES `users` (`Id_users`);

--
-- Contraintes pour la table `voter`
--
ALTER TABLE `voter`
  ADD CONSTRAINT `voter_ibfk_1` FOREIGN KEY (`Id_users`) REFERENCES `users` (`Id_users`),
  ADD CONSTRAINT `voter_ibfk_2` FOREIGN KEY (`Id_propositions`) REFERENCES `propositions` (`Id_propositions`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
