<? session_start();

require 'connect.php';
require 'partials/head.php';

//  requete qui sélectionne toute les info des proposition de lutilisateur connecter 

$pseudo= filter_input(INPUT_GET, 'pseudo');
$id=filter_input(INPUT_GET, 'id');
// print_r($id);
$query = $db->prepare("SELECT * FROM propositions WHERE Id_users=:Id_users");
    $query->execute(array(
        ':Id_users' => $id
    ));
    $propositions = $query->fetchAll(PDO::FETCH_ASSOC);
    //  echo "<pre>DEBUG";
    // print_r($propositions);
    // echo "</pre>";


    
    

    $query = $db->prepare("SELECT users.Id_users, propositions.Id_users, Id_propositions, pseudo, titre, pour,contre FROM users INNER JOIN propositions ON users.Id_users = propositions.Id_users ");
    $query->execute(array(
        
    ));
    $allPropo = $query->fetchAll(PDO::FETCH_ASSOC);
    //  echo "<pre>DEBUG";
    // print_r($allPropo);
    // echo "</pre>";

   
    // echo "<pre>DEBUG";
    // print_r($idProp);
    // echo "</pre>";


    // echo "<h4>PDO ERROR :</h4>";
    // echo "<pre>";
    // print_r($query->errorInfo());
    // echo "</pre>";

   
?>




<body style="background-color:#85929E  ;">
    
        <a href="create_prop.php?id=<?=$id?>"><button type="button" class="btn btn-outline-dark">ajouter</button></a>
    

    <nav>
        <h1>Bonjour <?=$pseudo?></h1> <!-- METTRE LE NOM DE LA PERSONNE CONNECTER  -->
        <a href="index.php">se déconnecter</a>

    </nav>

    <h3>Vos propositions </h3>
    <p>nombre de propositions : 0</p>
    <!--mettre le nombre de proposition egale au proposition afficher  -->
    <div class="shadow-lg p-3 mb-5 bg-body rounded ">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Titre</th>
                    <th scope="col">Soumise au vote</th>
                    <th scope="col">Pour</th>
                    <th scope="col">Contre</th>
                    <th scope="col">Modifier</th>
                    <th scope="col">Supprimer</th>
                </tr>
            </thead>
            <tbody>

                <!-- foreach qui encadre le le tr  -->

                <? foreach($propositions as $proposition){?>
                <tr>
                    <th scope="row"><?=$proposition['Id_propositions']?></th>
                    <td><?=$proposition['titre']?></td>
                    <? if($proposition['soumission']==false){?>
                    <td>non</td>
                    <?}
        else{?>
                    <td>oui</td>
                    <? }?>
                    <td><?=$proposition['pour']?></td>
                    <td><?=$proposition['contre']?></td>

                    <td><button type="button" class="btn btn-outline-dark">modifier</button></td>
                    <td><button type="button" class="btn btn-outline-dark">supp</button></td>
                </tr>


                <?}?>


            </tbody>
        </table>
    </div>



    <div class="shadow-lg p-3 mb-5 bg-body rounded ">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">utilisateur</th>
                    <th scope="col">Titre</th>
                    <th scope="col">Déjà voté</th>
                    <th scope="col">Pour</th>
                    <th scope="col">contre </th>
                    <th scope="col">voir </th>


                </tr>
            </thead>
            <tbody>

                <!-- foreach qui encadre le le tr  -->
                <? foreach($allPropo as $prop){?>
                 <?   $idPropo=$prop['Id_propositions'] ?>
                <tr>
                <th scope="row"><?=$prop['Id_propositions']?></th>
                <!-- utilisateur a afficher --><td> <?=$prop['pseudo']?></td>
                    <td><?=$prop['titre']?></td>
                    <td></td>
                    <td><?=$prop['pour']?></td>
                    <td><?=$prop['contre']?></td>
                    
                    <td><a href="propositions.php?idpropo=<?=$idPropo?>&id=<?=$id?>"><button type="button" class="btn btn-outline-dark">voir</button></a></td>
                </tr>
                <?}?>

            </tbody>
        </table>
    </div>


</body>