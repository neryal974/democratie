<?
require 'partials/head.php';


$action = filter_input(INPUT_GET, 'action');
$messages = [
    'noUser' => "Cet utilisateur n'existe pas",
    'noPassword' => "Le mot de passe n'est pas bon",
    'false' => "Les informations saisies sont incorectes !!"
];
$message = ($action) ? $messages[$action] : '';
?>



<body>
    <div id="container">
        <!-- zone de connexion -->

        <form action="verification.php" method="POST">
            <h1>Connexion</h1>

            <label><b>Nom d'utilisateur</b></label>
            <input type="text" placeholder="Entrer le nom d'utilisateur" name="username"  required>

            <label><b>Mot de passe</b></label>
            <input type="password" placeholder="Entrer le mot de passe" name="password"  required>

            <input type="submit" id='submit' value='LOGIN'>
            
        </form>
        <div class="alert alert-danger" role="alert"><?=$message?></div>
       
        
    </div><br><br><br>

<div>
    <form class="row g-3" action="createLogin.php" method="POST" style="width: 900px;  margin-left: 185px;">
        <div class="col-auto">
            <label for="staticEmail2" class="visually-hidden">pseudo</label>
            <input type="text" placeholder="pseudo"  class="form-control-plaintext"   name="pseudo">
        </div>

        <div class="col-auto">
            <label for="staticEmail2" class="visually-hidden">Email</label>
            <input type="text" placeholder="email" class="form-control-plaintext"   name="email">
        </div>
        <div class="col-auto">
            <label for="inputPassword2" class="visually-hidden">Password</label>
            <input type="password" class="form-control-plaintext"  placeholder="Password" name="password">
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary mb-3">S'inscrire</button>
        </div>
    </form>
    </div>

  
</body>